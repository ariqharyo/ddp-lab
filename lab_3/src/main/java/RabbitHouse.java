import java.util.Scanner;

public class RabbitHouse{
	public static int rabbitHouse(int len){
		if(len == 1){
			return 1;
		}
		else{
			return 1 + (rabbitHouse(len-1)*len);
		}
	}
		
	public static void main (String[] args){
		Scanner input = new Scanner(System.in);
			try{
				String rawInput = input.nextLine();
				String[] parts = rawInput.split(" ");
				String tipe = parts[0];
				String nama = parts[1];
				if (tipe.equals("normal")){
					int len = nama.length();
					System.out.println(rabbitHouse(len));
				}
			}
			catch(Exception e){
				System.out.println("Error! Ulangi!");
			}
		
	}
}
