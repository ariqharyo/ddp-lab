import character.*;
import java.util.ArrayList;

public class Game{
    public ArrayList<Player> player = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (int a = 0; a < player.size(); a++) {
            if(player.get(a).getName().equals(name)) return player.get(a);
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if(find(chara) == null) {
            if(tipe.equals("Human")) player.add(new Human(chara, hp));
            else if(tipe.equals("Magician")) player.add(new Magician(chara, hp));
            else if(tipe.equals("Monster")) player.add(new Monster(chara, hp));
            return chara + " ditambah ke game";
        } else return "Sudah ada karakter bernama " + chara;
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahk
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if(find(chara) == null){
            player.add(new Monster(chara, hp, roar));
            return chara + " ditambah ke game";
        }
        return "Sudah ada karakter bernama " + chara;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if(find(chara) == null) return "Tidak ada " + chara;
        player.remove(player.indexOf(find(chara)));
        return chara + " dihapus dari game"; 
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara){
        if(find(chara) == null) return "Tidak ada " + chara;
        return find(chara).status();
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status(){
        String allStatus = "";
        for(int a = 0; a < player.size(); a++) {
            allStatus += player.get(a).status();
        }
        return allStatus;        
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara){
        if(find(chara) == null) return "Tidak ada " + chara;
        if(find(chara).getDiet().size() == 0) return find(chara).getDietToString();
        else return find(chara).getDietToString().replace("Memakan ","");
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String allDiet = "";
        for(int a = 0; a < player.size(); a++) {
            if(player.get(a).getDiet().size() == 0) {
                allDiet += player.get(a).getDietToString();
            } else {
                allDiet += player.get(a).getDietToString().replace("Memakan ","");
        }
            } 
        return allDiet;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        if(find(meName) == null || find(enemyName) == null) {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
        if(find(meName).getIsAlive()){
            return find(meName).attack(find(enemyName)); 
        } else {
            return meName + " tidak bisa menyerang " + enemyName;
        } 
        
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        if(find(meName) == null || find(enemyName) == null) {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
        if(find(meName).getType().equals("Magician")){
            if(find(meName).getIsAlive()) {
                return ((Magician)find(meName)).burn(find(enemyName));
            } else {
                return meName + " tidak bisa membakar " + enemyName;
            }
        } else {
            return meName + " tidak bisa membakar";
        }
        
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        if(find(meName) == null || find(enemyName) == null) {
            if (find(meName) == null) System.out.println("ketang");
            return "Tidak ada " + meName + " atau " + enemyName;
        }
        if(find(meName).canEat(find(enemyName))) {
            Player x = find(enemyName);
            remove(enemyName);
            return find(meName).eat(x); 
        } else {
            return meName + " tidak bisa memakan " + enemyName;
        }
        
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        if(find(meName) == null) return "Tidak ada " + meName;
        if(find(meName).getType().equals("Monster")){
            return ((Monster)find(meName)).roar();
        }
        return meName + " tidak bisa berteriak";
    }
	
}