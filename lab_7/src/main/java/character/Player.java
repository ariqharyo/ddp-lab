package character;

import java.util.ArrayList;

//  write Player Class here
public class Player {
    private String name;
    private int HP;
    private ArrayList<Player> diet = new ArrayList<Player>();
    private String type;
    private boolean isAlive;
    private boolean isBurned = false;


    public Player(String name, String type, int HP) {
        this.name = name;
        this.type = type;
        if(HP > 0){
            this.HP = HP;
            this.isAlive = true;
        } 
        else{
            this.HP = 0;
            this.isAlive = false;
        } 
    }

    public String getName() {return this.name;}
    public void setName(String name) {this.name = name;}
    public int getHp() {return this.HP;}
    public void setHP(int HP) {
        if(HP <= 0) this.HP = 0; 
        else this.HP = HP;
    }
    public ArrayList<Player> getDiet() {return this.diet;}
    public String getType() {return this.type;}
    public void setType(String type) {this.type = type;}
    public boolean getIsAlive() {return this.isAlive;}
    public void setIsAlive(boolean isAlive) {this.isAlive = isAlive;}
    public boolean getIsBurned() {return this.isBurned;}
    public void setIsBurned(boolean isBurned) {this.isBurned = isBurned;} 
    public String getDietToString() {
        if(this.getDiet().size() == 0) return "Belum memakan siapa siapa";
        else {
            String dietToString = "";
            for(int a = 0; a < this.getDiet().size(); a++) {
                if(a == 0) dietToString += "Memakan"; 
                if(a == this.getDiet().size()-1){
                    dietToString += " " + this.getDiet().get(a).getType() + " " + this.getDiet().get(a).getName();
                } else {
                    dietToString += " " + this.getDiet().get(a).getType() + " " + this.getDiet().get(a).getName() + ", ";
                } 
            }
            return dietToString;
        }
    }

    public String attack(Player enemy) {
        if(enemy.getType().equals("Magician")) enemy.setHP(enemy.getHp()-20);
        else enemy.setHP(enemy.getHp()-10);
        if(enemy.getHp() == 0) enemy.setIsAlive(false);
        return "Nyawa " + enemy.getName() + " " + enemy.getHp();
    }

    public String eat(Player enemy) {
        this.diet.add(enemy);
        this.setHP(this.getHp() + 15);
        return this.getName() + " memakan " + enemy.getName() + "\n" +
               "Nyawa " + this.getName() + " kini " + this.getHp();
    }

    public String status() {
        String keterangan;
        if(getIsAlive()){
            keterangan = "Masih hidup";
        } else keterangan = "Sudah meninggal dunia dengan damai";
        return this.getType() + " " + this.getName() + "\n" +
               "HP: " + this.getHp() + "\n" +
               keterangan + "\n" +
               this.getDietToString();
               
    }

    public boolean canEat(Player othPlayer) {
        return true;

    }
}
