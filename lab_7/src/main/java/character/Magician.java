package character;

import javax.lang.model.util.ElementScanner8;

//  write Magician Class here
public class Magician extends Human {
    public Magician(String name, int HP) {
        super(name, "Magician", HP);
    }

    public String burn(Player enemy) {
        if(enemy.getType().equals("Magician") && enemy.getHp()-20 <= 0) {
            enemy.setIsBurned(true);
            return attack(enemy) + "\n" + "dan matang";
        } else if(!enemy.getType().equals("Magician") && enemy.getHp()-10 <= 0) {
            enemy.setIsBurned(true);
            return attack(enemy) + "\n" + "dan matang";
        } else return attack(enemy);
        
    }
}