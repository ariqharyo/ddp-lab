package character;

//  write Monster Class here
public class Monster extends Player {
    private String roar;

    public Monster(String name, int HP) {
        super(name, "Monster", 2*HP);
        this.setRoar("AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public Monster(String name, int HP, String roar) {
        super(name, "Monster", 2*HP);
        this.setRoar(roar);
    }
    private String getRoar() {return this.roar;}
    private void setRoar(String roar) {this.roar = roar;}

    public boolean canEat(Player enemy) {
        if(!enemy.getIsAlive()) {
            return true;
        } else return false;
    }

    public String eat(Player enemy){
        if(canEat(enemy)) {
            return super.eat(enemy);
        } else {
            return getName() + " tidak bisa memakan " + enemy.getName();
        }
    }

    public String roar() {
        return getRoar();
    }
}