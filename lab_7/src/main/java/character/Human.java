package character;

//  write Human Class here
public class Human extends Player{

    public Human(String name, int HP) {
        super(name, "Human", HP);
    }

    public Human(String name, String type, int HP) {
        super(name, type, HP);
    }

    public boolean canEat(Player enemy) {
        if(enemy.getType().equals("Monster") && enemy.getIsBurned()) {
            return true;
        } else return false;
    }

    public String eat(Player enemy) {
        if(canEat(enemy)) {
            return super.eat(enemy);
        } else {
            return getName() + " tidak bisa memakan " + enemy.getName();
        }
    }
}