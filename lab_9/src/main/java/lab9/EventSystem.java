package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.util.Date;

import java.text.SimpleDateFormat;
import java.text.ParseException;
/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }
    
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) throws ParseException
    {
        // TODO: Implement!
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            Date startTime = sdf.parse(startTimeStr);
            Date endTime = sdf.parse(endTimeStr);
            if(startTime.after(endTime))
            {
                return "Waktu yang diinputkan tidak valid!";
            }
            if(isEventNameNotExist(name))
            {
                events.add(new Event(name, startTimeStr, endTimeStr, costPerHourStr));
                return "Event " + name + " berhasil ditambahkan!";
            } else
            {
                return "Event " + name + " sudah ada!";
            }
        }
        catch(ParseException e) {
            e.printStackTrace();
            
        }
        return "";
        
            
        
    }
    
    public String addUser(String name)
    {
        // TODO: Implement:
        if(isUserNameNotExist(name)) {
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        } else {
            return "User " + name + " sudah ada!";
        }
        
    }
    
    public String registerToEvent(String userName, String eventName)
    {
        // TODO: Implement
        if(isUserNameNotExist(userName) && isEventNameNotExist(eventName))
        {
            return "Tidak ada pengguna dengan nama " + userName + 
                   " dan acara dengan nama " + eventName + "!";
        } else if(isUserNameNotExist(userName)) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if(isEventNameNotExist(eventName)) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        } else if(getUser(userName).addEvent(getEvent(eventName))) {
            return userName + " berencana menghadiri " + eventName + "!";
            //return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        } else {
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
            //return userName + " berencana menghadiri " + eventName + "!";
        }
    }

    public boolean isEventNameNotExist(String name) 
    {
        for(Event a : events) {
            if(name.equals(a.getName())) return false;
        }
        return true;
    }

    public boolean isUserNameNotExist(String name)
    {
        for(User b : users) {
            if(name.equals(b.getName())) return false;
        }
        return true;
    }

    public User getUser(String name) {
        for(User a : users) {
            if(a.getName().equals(name)){
                return a;
            }
        }
        return null;
    }

    public Event getEvent(String name) {
        for(Event a : events) {
            if(a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }

}