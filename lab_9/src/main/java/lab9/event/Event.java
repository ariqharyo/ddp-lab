package lab9.event;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.math.BigInteger;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    
    // TODO: Make instance variables for representing beginning and end time of event
    private String startTimeStr;
    private String endTimeStr;
    private Date startTime;
    private Date endTime;
    // TODO: Make instance variable for cost per hour
    private String cost;
    private BigInteger bigCost;

    SimpleDateFormat sdf; 
    

    
    // TODO: Create constructor for Event class

    public Event(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        this.name = name;
        this.startTimeStr = startTimeStr;
        this.endTimeStr = endTimeStr;
        this.cost = costPerHourStr;
        this.bigCost = new BigInteger(cost);
        try {
            this.sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            this.startTime = sdf.parse(startTimeStr);
            this.endTime = sdf.parse(endTimeStr);
        } catch(ParseException e) {
            e.printStackTrace();
        }
        
    }
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName()
    {
        return this.name;
    }

    public String getStartTimeStr()
    {
        return this.startTimeStr;
    }

    public String getEndTimeStr()
    {
        return this.endTimeStr;
    }

    public Date getStartTime()
    {
        return this.startTime;
    }

    public Date getEndTime()
    {
        return this.endTime;
    }

    public String getCost()
    {
        return this.cost;
    }

    public BigInteger getBigCost() 
    {
        return this.bigCost;
    }

    
    // TODO: Implement toString()
    
    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.

    public boolean overlapsWith(Event other) {
        if(this.endTime.equals(other.getStartTime()) || this.startTime.equals(other.getEndTime()))
        {
            return false;
        } 
        else if(this.endTime.before(other.getStartTime()) || other.getEndTime().before(this.startTime)) {
            return false;
        }
        else {
            return true;
        }
    }

    public String toString()
    {   
        //("yyyy-MM-dd_HH:mm:ss")
        sdf.applyPattern("dd-MM-yyyy, HH:mm:ss");
        String newStartTimeStr = sdf.format(startTime);
        String newEndTimeStr = sdf.format(endTime);
        return name + "\n" +
               "Waktu mulai: " + newStartTimeStr + "\n" +
               "Waktu selesai: " + newEndTimeStr + "\n" +
               "Biaya kehadiran: " + cost;
    }

    public int compareTo(Event compared)
    {
        return this.startTime.compareTo(compared.getStartTime());
    }
}
