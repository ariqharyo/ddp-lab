package customer;
import ticket.Ticket;
import theater.Theater;
import java.util.ArrayList;

public class Customer {
	//Instance variable
	private String name;
	private String gender;
	private int old;
	
	//Constructor
	public Customer(String name, String gender, int old) {
		this.name = name;
		this.gender = gender;
		this.old = old;
	}
	
	//Setter-Getter
	public String getName() {
		return this.name;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public String getGender(){
		return this.gender;
	}
	
	public void setGender(String newGender){
		this.gender = newGender; 
	}
	
	public int getOld(){
		return this.old;
	}
	
	public void setOld(int newOld){
		this.old = newOld;
	}
	
	//Method
	
	public Ticket orderTicket(Theater theater, String title, String day, String type){
		ArrayList<Ticket> listOfTickets = theater.getListOfTickets();
		Ticket ticket = null;
		for(int a = 0; a < listOfTickets.size(); a++){
			if(title.equals(listOfTickets.get(a).getTitle())
			   && day.equals(listOfTickets.get(a).getDay())
			   && type.equals(listOfTickets.get(a).getType())){
				if(this.getOld()>= listOfTickets.get(a).getMovie().getMinOld()) {
	 			   System.out.println(this.getName() + " telah membeli tiket " + title + " jenis " + type +
									  " di " + theater.getName() + " pada hari " + day + " seharga Rp. " + 
									  listOfTickets.get(a).getPrice());
	 			   theater.setRevenue(theater.getRevenue() + listOfTickets.get(a).getPrice());
				   return listOfTickets.get(a);
				} else {
					System.out.println(this.getName() + " masih belum cukup umur untuk menonton " + listOfTickets.get(a).getTitle() +
									  " dengan rating " + listOfTickets.get(a).getMovie().getRating());
				}
			}
			else if(a == listOfTickets.size()-1) {
				System.out.println("Tiket untuk film " + title +
						   " jenis " +  type + " dengan jadwal " + day +
						   " tidak tersedia di " + theater.getName());
			}
			//Kak Pewe telah membeli tiket Si Juki The Movie jenis Biasa di CGT Blitz pada hari Kamis seharga Rp. 60000
		}
		return ticket;
		//"Tiket untuk film Black Panther jenis 3 Dimensi dengan jadwal Senin tidak tersedia di CGT Blitz"
		//Tiket untuk film Dilan 1990 jenis Biasa dengan jadwal Selasa tidak tersedia di CompFest XXI
		//Sis Dea masih belum cukup umur untuk menonton Si Juki The Movie dengan rating Dewasa
		//Kak Pewe telah membeli tiket Si Juki The Movie jenis Biasa di CGT Blitz pada hari Kamis seharga Rp. 60000

	}

	public void findMovie(Theater theater, String title){
		for(int a = 0; a < theater.getListOfMovies().length; a++){
			if(title.equals(theater.getListOfMovies()[a].getTitle())){
				System.out.println(theater.getListOfMovies()[a]);
			} else if(a == theater.getListOfMovies().length - 1){
				System.out.println("Film " +  title + " yang dicari " + this.getName() + 
								   " tidak ada di bioskop " + theater.getName());
			}
		}
		
	}
}