package ticket;
import movie.Movie;

public class Ticket{
	//Instance variable
	private Movie movie;
	private String title;
	private String day;
	private String type;
	private boolean normalOr3D;
	private int price;
	
	//Constructor
	public Ticket(Movie movie, String day, boolean normalOr3D) {
		this.movie = movie;
		this.title = movie.getTitle();
		this.day = day;
		this.normalOr3D = normalOr3D;
		if(normalOr3D) {
			this.type = "3 Dimensi";
		} else {
			this.type = "Biasa";
		}
		
		if(this.day.equals("Sabtu") || this.day.equals("Minggu")) {
			if(normalOr3D) {
				this.price = 120000;
			} else {
				this.price = 100000;
			}
		} else {
			if(normalOr3D) {
				this.price = 72000;
			} else {
				this.price = 60000;
			}
		}
	}
	
	//Setter-Getter
	public Movie getMovie() {
		return this.movie;
	}
	
	public void setMovie(Movie newMovie) {
		this.movie = newMovie;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String newTitle) {
		movie.setTitle(newTitle);
	}
	
	public String getDay() {
		return this.day;
	}
	
	public void setDay(String newDay) {
		this.day = newDay;
	}
	
	public boolean getNormalOr3D() {
		return this.normalOr3D;
	}
	
	public void setNormalOr3D(boolean newNormalOr3D) {
		this.normalOr3D = newNormalOr3D;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String newType) {
		this.type = newType;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public void setPrice(int newPrice) {
		this.price = newPrice;
	}
	
	//Method toString()
	public String toString() {
		return  "------------------------------------------------------------------ \r\n" + 
				"Film            : " + this.getTitle() + "\r\n" +
				"Jadwal Tayang   : " + this.getDay() + "\r\n" +
				"Jenis           : " + this.getType() + "\r\n" +
				"------------------------------------------------------------------";


	}
	
}