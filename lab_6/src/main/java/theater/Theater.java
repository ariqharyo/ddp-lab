package theater;

import java.util.ArrayList;
import java.util.Arrays;
import movie.Movie;
import ticket.Ticket;

public class Theater {
	//Instance variable
	private String name;
	private int revenue;
	private static int totalRevenue;
	private ArrayList<Ticket> listOfTickets;
	private Movie[] listOfMovies;
	
	//Constructor
	public Theater(String name, int revenue, 
				   ArrayList<Ticket> listOfTickets, Movie[] listOfMovies) {
		this.name = name;
		this.revenue = revenue;
		this.listOfTickets = listOfTickets;
		this.listOfMovies = listOfMovies;
	}
	
	//Setter-Getter
	public String getName() {
		return this.name;
	}
	
	public void setName(String newName) {
		this.name = newName;
	}
	
	public int getRevenue() {
		return this.revenue;
	}
	
	public void setRevenue(int newRevenue) {
		this.revenue = newRevenue;
	}
	
	public ArrayList<Ticket> getListOfTickets(){
		return this.listOfTickets;
	}
	
	public void setListOfTickets(ArrayList<Ticket> newListOfTickets) {
		this.listOfTickets = newListOfTickets;
	}
	
	public Movie[] getListOfMovies() {
		return this.listOfMovies;
	}
	
	public void setListOfMovies(Movie[] newListOfMovies) {
		this.listOfMovies = newListOfMovies;
	}
	
	//Method
	public String printListOfMovies(Movie[] listOfMovies){
		String listMovies = "";
		for(int a = 0; a < this.getListOfMovies().length; a++) {
			if(a == this.getListOfMovies().length - 1) listMovies += this.getListOfMovies()[a].getTitle();
			else listMovies += this.getListOfMovies()[a].getTitle() + ", ";
		
		
		}
		return listMovies;
	}
	public void printInfo() {
		System.out.println("------------------------------------------------------------------ \r\n" +
						   "Bioskop                 : " + this.getName() + "\r\n" +
						   "Saldo Kas               : " + this.getRevenue() + "\r\n" +
						   "Jumlah tiket tersedia   : " + this.getListOfTickets().size() + "\r\n" +
						   "Daftar Film tersedia    : " + this.printListOfMovies(this.getListOfMovies()) + "\r\n" +
						   "------------------------------------------------------------------");

	}
	
	public static void printTotalRevenueEarned(Theater[] theaters) {
		int totalRevenue = 0;
		for(int a = 0; a < theaters.length; a++) {
			totalRevenue += theaters[a].getRevenue();
		}
		for(int a = 0; a < theaters.length; a++) {
			if(a == 0) {
				System.out.println(" Total uang yang dimiliki Koh Mas : Rp. " + totalRevenue +"\r\n" + 
								   "------------------------------------------------------------------\r\n" + 
								   "Bioskop         : " + theaters[a].getName() + "\r\n" + 
								   "Saldo Kas       : Rp. " + theaters[a].getRevenue() +  "\r\n" + 
								   " ");
			} else if(a == theaters.length - 1) {
				System.out.println("Bioskop         : " + theaters[a].getName() + "\r\n" + 
								   "Saldo Kas       : Rp. " + theaters[a].getRevenue() +  "\r\n" + 
						   		   "------------------------------------------------------------------");
			} else {
				System.out.println("Bioskop         : " + theaters[a].getName() + "\r\n" + 
						   		   "Saldo Kas       : Rp. " + theaters[a].getRevenue() +  "\r\n" + 
						           " ");
			}
		}
	}	
}