package movie;

public class Movie {
	//Instance variable
	private String title;
	private String rating;
	private int duration;
	private String genre;
	private String from;
	private int minOld;
	
	//Constructor
	public Movie(String title, String rating, int duration, 
				 String genre, String from) {
		this.title = title;
		this.rating = rating;
		this.duration = duration;
		this.genre = genre;
		this.from = from;
		if(this.rating.equals("Remaja")) this.minOld = 13;
		else if(this.rating.equals("Dewasa")) this.minOld = 17;
		else this.minOld = 0;
	}
	
	//Setter-Getter
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String newTitle) {
		this.title = newTitle;
	}
	
	public String getRating() {
		return this.rating;
	}
	
	public void setRating(String newRating) {
		this.rating = newRating;
	}
	
	public int getDuration() {
		return this.duration;
	}
	
	public void setDuration(int newDuration) {
		this.duration = newDuration;
	}
	
	public String getGenre() {
		return this.genre;
	}
	
	public void setGenre(String newGenre) {
		this.genre = newGenre;
	}
	
	public String getFrom() {
		return this.from;
	}
	
	public void setFrom(String newFrom) {
		this.from = newFrom;
	}
	
	public int getMinOld() {
		return this.minOld;
	}
	
	public void setMinOld(int newMinOld) {
		this.minOld = newMinOld;
	}
	
	//Method toString()
	public String toString() {
		return "------------------------------------------------------------------ \r\n" + 
				"Judul   : " + this.getTitle() + "\r\n" + 
				"Genre   : " + this.getGenre() + "\r\n" + 
				"Durasi  : " + this.getDuration() + " menit \r\n" + 
				"Rating  : " + this.getRating() + "\r\n" + 
				"Jenis   : Film " + this.getFrom() + "\r\n" + 
				"------------------------------------------------------------------";
	}
	
}