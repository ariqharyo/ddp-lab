//Import
import java.util.Scanner;
import java.util.Arrays;

public class BingoCard{
	//Instance Variable
	private String name;
	private String[][] row = new String[5][5];
	private String[][] column = new String[5][5];
	private String[][] diagonal = new String[2][5];
	private String[][] original = new String[5][5];
	private String[][] different = new String[5][5];
	private boolean allowRestart = true;
	private boolean stop = false;
	
	//Constructor
	public BingoCard(String name ,String[] arr0, String[] arr1, String[] arr2,
					 String[] arr3, String[] arr4){
		this.name = name;
		this.row[0] = arr0;
		this.row[1] = arr1;
		this.row[2] = arr2;
		this.row[3] = arr3;
		this.row[4] = arr4;
		for(int a = 0; a < 5; a++){
			for(int b = 0; b < 5; b++){
				this.setOriginal(a, b, this.getRow(a)[b]);
				this.setColumn(a, b , this.getRow(b)[a]);
				if(a == b){
					this.setDiagonal(0, a, this.getRow(a)[b]);
				}
				if(a + b == 4){
					this.setDiagonal(1, a, this.getRow(a)[b]);
				}
			}
		}
	}
	
	//Setter-getter
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String[] getRow(int a){
		return this.row[a];
	}
	
	public String[][] getRow(){
		return this.row;
	}
	
	public void setRow(int a, int b, String new_value){
		this.row[a][b] = new_value;
	}
	
	public String[] getColumn(int a){
		return this.column[a];
	}
	
	public void setColumn(int a, int b, String new_value){
		this.column[a][b] = new_value;
	}
	
	public String[] getDiagonal(int a){
		return this.diagonal[a];
	}
	
	public void setDiagonal(int a, int b, String new_value){
		this.diagonal[a][b] = new_value;
	}
	
	public String[] getOriginal(int a){
		return this.original[a];
	}

	public void setOriginal(int a, int b, String new_value){
		this.original[a][b] = new_value;
	}
	
	public String[] getDifferent(int a){
		return this.different[a];
	}
	
	public String[][] getDifferent(){
		return this.different;
	}
	
	public void setDifferent(int a, int b, String new_value){
		this.different[a][b] = new_value;
	}
	
	public boolean getAllowRestart(){
		return this.allowRestart;
	}
	
	public void setAllowRestart(boolean allowRestart){
		this.allowRestart = allowRestart;
	}
	
	public boolean getStop(){
		return this.stop;
	}
	
	public void setStop(boolean stop){
		this.stop = stop;
	}
	
	//Method
	public void markNum(String number){
		String[] bingo = new String[]{"X", "X", "X", "X", "X"};
		boolean gate = true;
		for (int a = 0; a < 5; a++){
			for(int b = 0; b < 5; b++){
				this.setDifferent(a, b, this.getRow(a)[b]);
				if(this.original[a][b].equals(number)){
					if(this.row[a][b].equals(number)){
						this.setRow(a, b, "X");
					}
					if(this.column[b][a].equals(number)){
						this.setColumn(b, a, "X");
						System.out.println(this.getName() + " : " + number + " tersilang");
					}
					else{
						System.out.println(this.getName() + " : " + number + " sebelumnya sudah tersilang");
						gate = false;
					}
				}
				else if(a == 4 && b == 4){
					if(Arrays.deepEquals(this.getDifferent(), this.getRow()) && gate){
						System.out.println(this.getName() + " : " + "Kartu tidak memiliki angka " + number);
					}
				}
				if(Arrays.equals(this.getRow(a), bingo)){
					System.out.println("BINGO!");
					this.info();
					this.setStop(true);
				}
				if(Arrays.equals(this.getColumn(b),bingo)){
					System.out.println("BINGO!");
					this.info();
					this.setStop(true);
				}
			}
			
			
		}
		for (int a = 0; a < 2; a++){
			for (int b = 0; b < 5; b++){
				if(this.diagonal[a][b].equals(number)){
					this.setDiagonal(a, b, "X");
				}
			}
			if(Arrays.equals(this.getDiagonal(a),bingo)){
				System.out.println("BINGO!");
				this.info();
				this.setStop(true);
			}
		}
	}
	
	public void restart(){
		if(this.getAllowRestart()){
			for(int a = 0; a < 5; a++){
				for(int b = 0; b < 5; b++){
					this.setRow(a, b, this.getOriginal(a)[b]);
					this.setColumn(a, b , this.getOriginal(b)[a]);
					if(a == b){
						this.setDiagonal(0, a, this.getOriginal(a)[b]);
					}
					if(a + b == 4){
						this.setDiagonal(1, a, this.getOriginal(a)[b]);
					}
				}
			}
			System.out.println("Mulligan!");
			this.setAllowRestart(false);
		}
		else{
			System.out.println(this.getName() + " sudah pernah mengajukan RESTART");
		}
	}
	
	public void info(){
		System.out.println(this.getName());
		for(int a = 0; a < 5; a++){
			for(int b = 0; b < 5; b++){
				if(b == 4){
					System.out.println("| " + this.getRow(a)[b] + " |");
				}
				else{
					System.out.print("| " + this.getRow(a)[b] + " ");
				}
			}
		}
	}
	
	//Main
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		String[] numAndName = input.nextLine().split(" ");
		int numPlayer = Integer.parseInt(numAndName[0]);
		String[] playerName = Arrays.copyOfRange(numAndName, 1, numAndName.length);
		BingoCard[] playerList = new BingoCard[numPlayer];
		
		
		for(int a = 0; a < numPlayer; a++){
			String[] arr0 = input.nextLine().split(" ");
			String[] arr1 = input.nextLine().split(" ");
			String[] arr2 = input.nextLine().split(" ");
			String[] arr3 = input.nextLine().split(" ");
			String[] arr4 = input.nextLine().split(" ");
			playerList[a] = new BingoCard(playerName[a] ,arr0, arr1, arr2, arr3, arr4);
		}
			
		while(true){
			String rawInput = input.nextLine();
			if(rawInput.split(" ")[0].equals("RESTART")){
				for(int a = 0; a < numPlayer; a++){
					if(playerList[a].getName().equals(rawInput.split(" ")[1])){
						playerList[a].restart();
					}
				}
			}
			else if(rawInput.split(" ")[0].equals("INFO")){
				for(int a = 0; a < numPlayer; a++){
					if(playerList[a].getName().equals(rawInput.split(" ")[1])){
						playerList[a].info();
					}
				}
			}
			else if(rawInput.split(" ")[0].equals("MARK")){
				for(int a = 0; a < numPlayer; a++){
					playerList[a].markNum(rawInput.split(" ")[1]);
				}
				for(int a = 0; a < numPlayer; a++){
					if(playerList[a].getStop()){
						input.close();
						System.exit(0);
					}
				}
			}
			else{
				System.out.println("Incorrect command");
			}
		}
		
	}	
}