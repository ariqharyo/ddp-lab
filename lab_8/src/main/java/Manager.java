public class Manager extends Karyawan {

    public Manager(String nama, int gaji) {
        this.nama = nama;
        this.gaji = gaji;
        this.kedudukan = "MANAGER";
    }

    public boolean bawahanable(Karyawan bawahan) {
        if(bawahan.getKedudukan().equals("STAFF") || bawahan.getKedudukan().equals("INTERN")) {
            return true;
        } else return false;
    }
}