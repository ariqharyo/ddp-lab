public class Staff extends Karyawan {

    public Staff(String nama, int gaji) {
        this.nama = nama;
        this.gaji = gaji;
        this.kedudukan = "STAFF";
    }

    public boolean bawahanable(Karyawan bawahan) {
        if(bawahan.getKedudukan().equals("INTERN")) {
            return true;
        } else return false;
    }
}