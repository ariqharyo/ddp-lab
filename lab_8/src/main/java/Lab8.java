import java.util.ArrayList;
import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {
        String nama;
        String kedudukan;
        int gaji;
        String perekrut;
        String terekrut;
        Scanner input = new Scanner(System.in);
        Korporasi korporasi = new Korporasi();
        korporasi.limitGaji = Integer.parseInt(input.nextLine());
        while(true) {
            String[] input_split = input.nextLine().split(" ");
            if(input_split[0].equals("TAMBAH_KARYAWAN")) {
                nama= input_split[1];
                kedudukan = input_split[2];
                gaji = Integer.parseInt(input_split[3]);
                korporasi.tambahKaryawan(nama, gaji, kedudukan);
            } else if(input_split[0].equals("STATUS")) {
                nama = input_split[1];
                korporasi.status(nama);
            } else if(input_split[0].equals("TAMBAH_BAWAHAN")) {
                perekrut = input_split[1];
                terekrut = input_split[2];
                korporasi.rekrutBawahan(perekrut, terekrut);
            } else if(input_split[0].equals("GAJIAN")) {
                korporasi.gajian();
            } else if(input_split[0].equals("EXIT")) {
                break;
            } else {
                System.out.println("Instruksi tidak tepat");
            }
        }
        input.close();

    }
}