import java.util.ArrayList;

public class Korporasi {
    public ArrayList<Karyawan> listKaryawan = new ArrayList<Karyawan>();
    public int limitGaji = 0;

    public Karyawan findKaryawan(String nama) {
        for(int a = 0; a < listKaryawan.size(); a++) {
            if(listKaryawan.get(a).getNama().equals(nama)) {
                return listKaryawan.get(a);
            }
        } return null;
    }

    public Karyawan findBawahan(String nama, Karyawan karyawan) {
        for(int a = 0; a < karyawan.getListBawahan().size(); a++) {
            if(karyawan.getListBawahan().get(a).getNama().equals(nama)) {
                return karyawan.getListBawahan().get(a);
            }
        } return null;
    }

    public void removeKaryawan(String nama) {
        listKaryawan.remove(listKaryawan.indexOf(findKaryawan(nama)));
    }

    public void tambahKaryawan(String nama, int gaji, String kedudukan) {
        if(listKaryawan.size() <= 10000) {
            if(findKaryawan(nama) == null) {
                if(kedudukan.equals("MANAGER")){
                    listKaryawan.add(new Manager(nama, gaji));
                } else if(kedudukan.equals("STAFF")) {
                    listKaryawan.add(new Staff(nama, gaji));
                } else if(kedudukan.equals("INTERN")) {
                    listKaryawan.add(new Intern(nama, gaji));
                }
                System.out.println(nama + " mulai bekerja sebagai " + 
                                   findKaryawan(nama).getKedudukan() + 
                                   " di PT. TAMPAN");
            } else System.out.println("Karyawan dengan nama " + nama + " telah terdaftar"); 
        } else System.out.println("Korporasi tidak dapat menerima karyawan lagi");
         
    }

    public void promosi(Karyawan staff) {
        String nama = staff.getNama();
        int gaji = staff.getGaji();
        ArrayList<Karyawan> listBawahan = staff.getListBawahan();
        int gajianCounter = staff.getGajianCounter();
        removeKaryawan(nama);
        listKaryawan.add(new Manager(nama, gaji));
        findKaryawan(nama).setListBawahan(listBawahan);
        findKaryawan(nama).setGajianCounter(gajianCounter);
        System.out.println("Selamat, " + nama + " telah dipromosikan menjadi MANAGER");

    }

    public void gajian() {
        System.out.println("Semua karyawan telah diberikan gaji");
        for(int a = 0; a < listKaryawan.size(); a++) {
            listKaryawan.get(a).gajian();
            if(listKaryawan.get(a).getGajianCounter() % 6 == 0) {
                listKaryawan.get(a).naikGaji();
            }
            if(listKaryawan.get(a).getKedudukan().equals("STAFF")) {
                if(listKaryawan.get(a).getGaji() > limitGaji) {
                    promosi(listKaryawan.get(a));
                }
            }
        }
    }

    public void rekrutBawahan(String perekrut, String terekrut) {
        if(findKaryawan(perekrut) != null && findKaryawan(terekrut) != null) {
            if(findBawahan(terekrut, findKaryawan(perekrut)) == null) {
                findKaryawan(perekrut).rekrutBawahan(findKaryawan(terekrut));
            } else System.out.println("Karyawan " + terekrut + " telah menjadi bawahan " + perekrut);
        } else System.out.println("Nama tidak berhasil ditemukan");
    }

    public void status(String nama) {
        if(findKaryawan(nama) != null) {
            findKaryawan(nama).status(); 
        } else System.out.println("Nama tidak berhasil ditemukan");
        
    }
}