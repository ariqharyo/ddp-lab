import java.util.ArrayList;

public abstract class Karyawan extends Korporasi {
    protected String nama;
    protected int gaji;
    protected String kedudukan;
    protected ArrayList<Karyawan> listBawahan = new ArrayList<Karyawan>();
    protected int gajianCounter = 0;

    public String getNama() {return this.nama;}
    public int getGaji() {return this.gaji;}
    public void setGaji(int gaji) {this.gaji = gaji;}
    public String getKedudukan() {return this.kedudukan;}
    public void setKedudukan(String kedudukan) {this.kedudukan = kedudukan;}
    public int getGajianCounter() {return this.gajianCounter;}
    public void setGajianCounter(int gajianCounter) {this.gajianCounter = gajianCounter;}
    public ArrayList<Karyawan> getListBawahan() {return this.listBawahan;}
    public void setListBawahan(ArrayList<Karyawan> listBawahan) {this.listBawahan = listBawahan;}
    
    public void gajian() {
        gajianCounter += 1;
    }

    public void naikGaji() {
        int gajiAwal = gaji;
        int gajiAkhir = gaji*11/10;
        gaji += gaji/10;
        System.out.println(nama + " mengalami kenaikan gaji sebesar 10% dari " 
                           + gajiAwal + " menjadi " + gajiAkhir); 
    }

    public void rekrutBawahan(Karyawan bawahan) {
        if(listBawahan.size() < 10) {
            if(bawahanable(bawahan)) {
                listBawahan.add(bawahan);
                System.out.println("Karyawan " + bawahan.getNama() + 
                                   " telah menjadi bawahan " + nama);
            } else {
                System.out.println("Anda tidak layak memiliki bawahan"); 
            }
        } else System.out.println("Anda tidak dapat menambah merekrut bawahan baru");
    }
    public abstract boolean bawahanable(Karyawan bawahan);

    public void status() {
        System.out.println(nama + " " + gaji);
    }

}