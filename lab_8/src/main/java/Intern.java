public class Intern extends Karyawan {

    public Intern(String nama, int gaji) {
        this.nama = nama;
        this.gaji = gaji;
        this.kedudukan = "INTERN";
    }

    public boolean bawahanable(Karyawan bawahan) {
        return false;
    }
}