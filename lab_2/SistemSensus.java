import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author: Ariq Haryo Setiaki, NPM: 1706043853, Kelas: C, GitLab Account: https://gitlab.com/ariqharyo
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		short panjang =  Short.parseShort(input.nextLine()); //input.nextShort();
		} 
		System.out.print("Lebar Tubuh (cm)       : ");
		short  lebar = Short.parseShort(input.nextLine()); //input.nextShort(); 
		if (lebar <=0 || lebar > 250){
			System.out.print("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		} 
		System.out.print("Tinggi Tubuh (cm)      : ");
		short tinggi =  Short.parseShort(input.nextLine()); //input.nextShort();
		if (tinggi <= 0 || tinggi > 250){
			System.out.print("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		} 
		System.out.print("Berat Tubuh (kg)       : ");
		float berat = Float.parseFloat(input.nextLine()); //input.nextFloat()
		if (berat <= 0 || berat > 150){
			System.out.print("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Jumlah Anggota Keluarga: ");
		byte makanan = Byte.parseByte(input.nextLine()); //input.nextByte();
		if (makanan <= 0 || makanan > 20){
			System.out.print("WARNING : Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = "Catatan : " + input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		int jumlahCetakan =  Integer.parseInt(input.nextLine()); //input.nextInt();


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		float x = 100;
		int rasio =  (int)(berat/((panjang/x)*(lebar/x)*(tinggi/x)));

		for (int a = 1; a <= jumlahCetakan; a++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("\nPencetakan " + a + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.length() == 10) catatan = "Tidak ada catatan tambahan";
			else catatan = catatan;

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima + "\n" +
						   "-----------------\n" +
						   nama + " - " + alamat + "\n" +
						   "Lahir pada tanggal " + tanggalLahir + "\n" +
						   "Rasio Berat Per Volume = " + rasio + " kg/m^3\n" +
						   catatan;
			System.out.println(hasil);
		}


		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int jumlahAscii = 0;
		char[] listKarakter = nama.toCharArray();
		for (int i = 0; i < listKarakter.length; i++){
			char karakter = listKarakter[i];
			int ascii = (int) karakter;
			jumlahAscii += ascii;
		}
		
		String hasilKalkulasi = String.valueOf((((panjang*tinggi*lebar) + jumlahAscii) % 10000));

		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomor = nama.charAt(0) + hasilKalkulasi;


		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (int) (50000*365*makanan); //50000*365*20 = 365000000 *365 juta
		

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		short tahunLahir = Short.parseShort(tanggalLahir.split("-")[2]); // lihat hint jika bingung
		short umur = (short) (2018 - tahunLahir);
		

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String apartemen = "";
		String kabupaten = "";
		if (umur <= 18){
			apartemen += ("PPMT");
			kabupaten += ("Rotunda");
		}
		else{
			if (anggaran <= 100000000){
				apartemen += ("Teksas");
				kabupaten += ("Sastra");
			
			}
			else{
				apartemen += ("Mares");
				kabupaten += ("Margonda");
			}
		}
		
		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "\nREKOMENDASI APARTEMEN\n" +
							 "-----------------\n" +
							 "MENGETAHUI: Indentitas keluarga: " + nama + " - " + nomor + "\n" +
							 "MENIMBANG: Anggaran makanan tahunan : Rp " + String.valueOf(anggaran) + "\n" +
							 "           Umur kepala keluarga: " + umur + " tahun\n" +
							 "MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di:\n" +
							 apartemen + ", kabupaten " + kabupaten;
		System.out.println(rekomendasi);

		input.close();
	}
}