public class Manusia{
	
	private String nama;
	private int umur;
	private int uang;
	private float kebahagiaan;
	private boolean hidupMati;
	public static Manusia orangTerakhir;
	
	public Manusia(String nama, int umur, int uang){
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
		this.kebahagiaan = 50;
		this.hidupMati = true;
		orangTerakhir = this;		
	}
	
	public Manusia(String nama, int umur){
		this.nama = nama;
		this.umur = umur;
		this.uang = 50000;
		this.kebahagiaan = 50;
		this.hidupMati = true;
		orangTerakhir = this;
	}
	
	public void setNama(String nama){
		this.nama = nama;
	}
	
	public String getNama(){
		return nama;
	}
	
	public void setUmur(int umur){
		this.umur = umur;
	}
	
	public int getUmur(){
		return umur;
	}
	
	public void setUang(int uang){
		this.uang = uang;
	}
	
	public int getUang(){
		return uang;
	}
	
	public void setKebahagiaan(float kebahagian){
		
		this.kebahagiaan = kebahagian;
	}
	
	public float getKebahagiaan(){
		return kebahagiaan;
	}
	
	public void setHidupMati(boolean hidupMati){
		this.hidupMati = hidupMati;
	}
	
	public boolean getHidupMati(){
		return hidupMati;
	}
	
	public void beriUang(Manusia penerima){
		if ((hidupMati) && (penerima.getHidupMati())){
			int jumlahAscii = 0;
			for (int a = 0; a < penerima.getNama().length(); a++){
				char karakter = penerima.getNama().charAt(a);
				int ascii = (int) (karakter);
				jumlahAscii += ascii;
			}
			int jumlahUang = jumlahAscii*100;
			float p = 6000;
			float tambahKebahagiaan = (float) (jumlahUang/p);
			if (uang - jumlahUang >= 0){
				this.setUang(uang - jumlahUang);
				penerima.setUang(penerima.getUang() + jumlahUang);
				if(kebahagiaan + tambahKebahagiaan > 100){
					if (penerima.getKebahagiaan() + tambahKebahagiaan > 100){
						this.setKebahagiaan(100);
						penerima.setKebahagiaan(100);
					}
					else{
						this.setKebahagiaan(100);
						penerima.setKebahagiaan(penerima.getKebahagiaan() 
						+ tambahKebahagiaan);
					}
				}
				else if (penerima.getKebahagiaan() + tambahKebahagiaan > 100){
					this.setKebahagiaan(kebahagiaan + tambahKebahagiaan);
					penerima.setKebahagiaan(100);
				}
				else{
					this.setKebahagiaan(kebahagiaan + tambahKebahagiaan);
					penerima.setKebahagiaan(penerima.getKebahagiaan() + tambahKebahagiaan);
				}
			System.out.println(nama + " memberi uang sebanyak " +
			jumlahUang + " kepada " + penerima.getNama() + " mereka berdua senang :D");
			}
			else{
				System.out.println(nama + " ingin memberi uang kepada " +
			penerima.getNama() + " namun tidak memiliki cukup uang :'(");
			}
		}
		else if((hidupMati) && !(penerima.getHidupMati())){
			System.out.println(penerima.getNama() + " telah tiada");
		}
		else if(!(hidupMati) && (penerima.getHidupMati())){
			System.out.println(this.getNama() + " telah tiada");
		}
		else{
			System.out.println("Dua - duanya wes modar Mas");
		}
	}
	
	public void beriUang(Manusia penerima, int jumlah){
		if ((hidupMati) && (penerima.getHidupMati())){
			float p = 6000;
			float tambahKebahagiaan = (float) (jumlah/p);
			if (uang - jumlah >= 0){
				this.setUang(uang - jumlah); //
				penerima.setUang(penerima.getUang() + jumlah);
				if(kebahagiaan + tambahKebahagiaan > 100){
					if (penerima.getKebahagiaan() + tambahKebahagiaan > 100){
						this.setKebahagiaan(100);
						penerima.setKebahagiaan(100);
					}
					else{
						this.setKebahagiaan(100);
						penerima.setKebahagiaan(penerima.getKebahagiaan() 
						+ tambahKebahagiaan);
					}
				}
				else if (penerima.getKebahagiaan() + tambahKebahagiaan > 100){
					this.setKebahagiaan(kebahagiaan + tambahKebahagiaan);
					penerima.setKebahagiaan(100);
				}
				else{
					this.setKebahagiaan(kebahagiaan + tambahKebahagiaan);
					penerima.setKebahagiaan(penerima.getKebahagiaan() + tambahKebahagiaan);
				}
			System.out.println(nama + " memberi uang sebanyak " +
			jumlah + " kepada " + penerima.getNama() + " mereka berdua senang :D");
			}
			else{
				System.out.println(nama + " ingin memberi uang kepada " +
			penerima.getNama() + " namun tidak memiliki cukup uang :'(");
			}
		}
		else if((hidupMati) && !(penerima.getHidupMati())){
			System.out.println(penerima.getNama() + " telah tiada");
		}
		else if((hidupMati) && (penerima.getHidupMati())){
			System.out.println(nama + " telah tiada");
		}
		else{
			System.out.println("Dua - duanya wes modar Mas");
		}
	}
	
	public void bekerja(int durasi, int bebanKerja){
		if(hidupMati){
			int pendapatan;
			int bebanKerjaTotal = durasi*bebanKerja;
			if (umur < 18){
				System.out.println(nama + " belum boleh bekerja karena masih dibawah umur D:");
			}
			else{
				if (bebanKerjaTotal <= kebahagiaan){
					this.setKebahagiaan(kebahagiaan - bebanKerjaTotal);
					pendapatan = bebanKerjaTotal*10000;
					System.out.println(nama + " bekerja full time, total pendapatan : " + pendapatan);
				}
				else{
					int durasiBaru = (int) (kebahagiaan/bebanKerja);
					int BebanKerjaTotal = durasiBaru*bebanKerja;
					pendapatan = BebanKerjaTotal*10000;
					this.setKebahagiaan(kebahagiaan - BebanKerjaTotal);
					System.out.println(nama + 
					" tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " 
					+ pendapatan);
				}
				this.setUang(uang + pendapatan);
			}
		}
		else{
			System.out.println(nama + " telah tiada");
		}
	}

	
	public void rekreasi(String namaTempat){
		if (hidupMati){
			int biaya = namaTempat.length()*10000;
			float TambahKebahagiaan = (float) (namaTempat.length());
			if (uang - biaya >= 0){
				this.setUang(uang - biaya);
				if(kebahagiaan + TambahKebahagiaan > 100){
					this.setKebahagiaan(100);
				}
				else{
					this.setKebahagiaan(kebahagiaan + TambahKebahagiaan);
				}
				System.out.println(nama + " berekreasi di " + namaTempat
				+ ", " + nama + " senang :)");
			}
			else{
				System.out.println(nama + " tidak mempunyai cukup uang untuk berekreasi di "
				+ namaTempat); 
			}
		}
		else{
			System.out.println(nama + " telah tiada");
		}
	}
	
	public void sakit(String namaPenyakit){
		if (hidupMati){
			float kurangKebahagiaan = (float) (namaPenyakit.length());
			if (kebahagiaan - kurangKebahagiaan >= 0){
				this.setKebahagiaan(kebahagiaan - kurangKebahagiaan);
			}
			else{
				this.setKebahagiaan(0);
			}
			System.out.println(nama + " terkena penyakit " + namaPenyakit + " :O");
		}
		else{
			System.out.println(nama + " telah tiada");
		}
	}
	
	public void meninggal(){
		if (hidupMati){
			this.setHidupMati(false);
			System.out.println(nama + " meninggal dengan tenang, kebahagiaan : " + kebahagiaan);
			if (this == orangTerakhir){
				this.setUang(0);
				System.out.println("Semua harta " + nama + " hangus");
			}
			else{
				orangTerakhir.setUang(orangTerakhir.getUang()+ uang);
				this.setUang(0);
				System.out.println("Semua harta " + nama + " disumbangkan untuk " 
				+ orangTerakhir.getNama());
			}
		}
		else{
			System.out.println(nama + " telah tiada");
		}	
	}
	
	public String toString(){
		if(hidupMati){
			return "Nama\t\t: " + nama + "\n" +
				   "Umur\t\t: " + umur + "\n" +
			       "Uang\t\t: " + uang + "\n" +
			       "Kebahagiaan\t: " + kebahagiaan;
		}
		else{
			return "Nama\t\t: Almarhum " + nama + "\n" +
				   "Umur\t\t: " + umur + "\n" +
			       "Uang\t\t: " + uang + "\n" +
			       "Kebahagiaan\t: " + kebahagiaan;
		}
	}
}

	